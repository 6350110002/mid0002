import 'package:bmi_usethis/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ResultScreen extends StatelessWidget {

  final bmiModel;

  ResultScreen({this.bmiModel});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          width: double.infinity,
          height: double.infinity,
          padding: EdgeInsets.all(32),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[

              Container(
                height: 200,
                width: 200,
                child: bmiModel.isNormal ? SvgPicture.asset("assets/images/happy.svg", fit: BoxFit.contain,) : SvgPicture.asset("assets/images/sad.svg", fit: BoxFit.contain,) ,
              ),
              SizedBox(
                height: 8,
              ),
              //แทรกข้อความ Your BMI is XXXX
              Text('Your BMI is ${bmiModel.bmi.round()}',
                  style: TextStyle(color: Colors.red,fontWeight: FontWeight.w700,fontSize:34)),
              //แทรกข้อความ comments ใน bmiModel.comments
              Text('You are $bmiModel',style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w500,fontSize: 18)),
              SizedBox(height: 16,),
              //แทรกข้อความ Hurray! Your BMI is Normal. กรณีที่ bmiModel.isNormal เป็นจริง
              //หรือแทรกข้อความ Sadly! Your BMI is not Normal. กรณีที่ bmiModel.isNormal เป็นเท็จ
              //โดยใช้วิธีการกำหนดเงื่อนไขของ bmiModel.isNormal ดูตัวอย่าง บรรทัดที่ 24 ด้านบน
              SizedBox(height: 16,),
              Container(
                child: ElevatedButton.icon(
                  onPressed: (){
                    Navigator.push(context,MaterialPageRoute(builder:(context) {
                      return MyApp();
                    }, ));
                    //แทรกโค้ดให้คลิกแล้วกลับไปหน้าแรก
                  },
                  icon: Icon(Icons.arrow_back_ios, color: Colors.white,),
                  label: Text("LET CALCULATE AGAIN"),
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(Colors.pink),
                  ),
                  //ปรับสีปุ่มให้เป็นสีชมพู

                ),
                width: double.infinity,
                padding: EdgeInsets.only(left: 16, right: 16),
              )

            ],
          ),
        )
    );
  }
}